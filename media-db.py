# Mediadb
# Write media owned to an sqlite3 database
#
# Copyright 2017 - Adam Cripps adam@monkeez.org
# Distributed under the GPL license Version 2 June 1991 - Please see LICENSE file for more information about distribution rights. 

import sqlite3
import sys
import os.path
from datetime import date, datetime, time
from time import gmtime, strftime
import time

def adapt_datetime(ts):
    return time.mktime(ts.timetuple())

def openDB(dbname):
    filename = dbname + '.db'
    con = sqlite3.connect(filename, detect_types=sqlite3.PARSE_DECLTYPES)
    #mycursor = con.cursor()
    #con = sqlite3.Connection(sqlite3.connect(filename))
    return
        
def additem(dbname):
    now = datetime.now()
    now = adapt_datetime(now)
    sqlite3.register_adapter(datetime, adapt_datetime)
    db = sqlite3.connect(dbname + '.db', detect_types=sqlite3.PARSE_DECLTYPES)
    print ("we've opened a db ", db)
    mycursor = db.cursor()
    print (now)
    title = input("Title: ")
    location = input("Location: ")
    mediatype = input("Media type: ")
    mycursor.execute("INSERT INTO "+dbname+" VALUES (?, ?, ?, ?)", (now, title, location, mediatype))
    db.commit()
    db.close()

def createDatabase(dbname):
    print ("You want to create a database called ", dbname)
    dbfilename = dbname+'.db'
    con = sqlite3.connect(dbfilename, detect_types=sqlite3.PARSE_DECLTYPES)
    print ("Created a database called ", dbname)
    if os.path.isfile(dbname) == True:
        print ("That database already exists - try again ")
        newname = input("Choose a new database name: ")
        createDatabase(newname)
    else:
        mycursor = con.cursor()
        mycursor.execute("CREATE TABLE "+dbname+" (date LONG, title, location, mediatype)")
        con.commit()
        con.close()
        wherenext = input("Add items to this database? y/n ")
        if wherenext =="y":
            additem(dbname)
        else:
            return

def quitme(status):
    if status == True:
        print("Goodbye")
        sys.exit(0)
    else:
        MenuItem()

def printResult(answer):
    answer = answer.fetchone()
    date = datetime.fromtimestamp(answer[0] /1e3)
    print("Date:" +str(date))
    i = 1
    while i<4:
        if i == 1:
            print ("Title: "+ answer[i])
        elif i == 2:
            print ("Location: "+answer[i])
        elif i == 3:
            print ("Media type: "+answer[i])            
        i=i+1
    print ("\n")

def searchDBfield(dbname, dbfield, userquery):
    # This is a general search function. It will take the name of the search and the field to search and sends the cursor to printResult 
    dbfilename = dbname + ".db"
    con = sqlite3.connect(dbfilename, detect_types=sqlite3.PARSE_DECLTYPES)
    mycursor=con.cursor()
    mycursor.execute("SELECT * FROM " +dbname+ " where "+ dbfield+ "='"+userquery+"'")
    #print ("Results returned: " + str(answer))
    printResult(mycursor)

def searchTitles(dbname):
    # This searches the title of the media
    userquery = input("Title search term: ")
    print("\n")
    searchDBfield(dbname, "title", userquery)
    
def searchMediaTypes(dbname):
    # This searches the type of media (e.g. CDROM or download)
    userquery = input("Media Type search term: ")
    print("\n")
    searchDBfield(dbname, "mediatype", userquery)

def searchLocations(dbname):
    # This searches the place that the user stores the media
    userquery = input("Location search term: ")
    print("\n")
    searchDBfield(dbname, "location", userquery)

def exportDB(dbname):
    # Write the whole of the DB to a file (possibly CSV-like).
    pass

def printDB(dbname):
    # Print the whole of the DB
    pass

def dbstats(dbname):
    # Returns how many records are in the database. 
    pass

def importFile(dbname):
    # This function will import a simple CSV like file that will import media - enabling the user to prepare a file speeds up importing and potential copying and pasting from a website. 
    pass

def queryDB(dbname):
    # THis is causing a bug - I'm using dbname without the prefix of .db and the table set up is withouth the db. 
    menu = {}
    menu['[1]']="Add a new item"
    menu['[2]']="Search titles"
    menu['[3]']="Search locations"
    menu['[4]']="Search media types"
    menu['[5]']="Import file"
    menu['[6]']="Export database"
    menu['[7]']="Print database"
    menu['[8]']="Database stats"
    menu['[9]']="Back to the main menu"
    
    while True:
        options=menu.keys()
        sortedoptions = sorted(options)
        for entry in sortedoptions:
            print (entry, menu[entry])
            if entry =="[4]":
                print ("\n")
                
        selection = input("Please Select:")
        if selection=='1':
            additem(dbname)
        elif selection=='2':
            searchTitles(dbname)
        elif selection=='3':
            searchLocations(dbname)
        elif selection=='4':
            searchMediaTypes(dbname)
        elif selection=='5':
            importFile(dbname)
        elif selection=='6':
            exportDB(dbname)
        elif selection=='7':
            printDB(dbname)
        elif selection=='8':
            dbstats(dbname)
        elif selection=='9':
            MenuItem()
        else:
            queryDB(dbname)

            
    
        
def MenuItem():
    menu = {}
    menu['[1]']="Open Database"
    menu['[2]']="Save Database"
    menu['[3]']="New Database"
    menu['[4]']="Quit program"
    while True:
        options=menu.keys()
        sortedoptions = sorted(options)
        for entry in sortedoptions:
            print (entry, menu[entry])
        if 'dbname' not in locals():
            print ("No database opened.")
        else:
            print ("Opened database: " + dbname)

        print ("\n")
        selection = input("Please Select:")
        if selection=='1':
            dbname = input("What database would you like to open?: ")
            openDB(dbname)
            queryDB(dbname)
        elif selection=='2':
            saveDB(dbname)
        elif selection=='3':
            dbname = input("Please name the database: ")
            createDatabase(dbname)
        elif selection=='4':
            confirm = input("You want to quit? Y to confirm: ")
            if confirm == "Y":
                quitme(True)
                break
            else:
               MenuItem()
class db:
    def __init__(self):
      MenuItem()

if __name__ == "__main__":
    mydb = db() 
